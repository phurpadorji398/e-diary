var email = localStorage.getItem("email")

window.onload = function() {
    fetch("/profile/" +email)
    .then(res => res.text())
    .then(data => showData(data))
}


document.querySelector("#picture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})


function save() {
    var data = {
        name: document.querySelector(".n").value,
        job: document.querySelector(".j").value,
        uimg: localStorage.getItem("image-dataurl")
    }

    console.log(data)
    fetch("/user/"+email, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: { "Content-type": "application/json; charset=UTF-8" }

    })
    .then((response) => {
        if (response.ok) {
            window.open("profile.html", "_self")
          
        } else {
          throw new Error("Failed to delete book");
        }
      })
      .catch((error) => {
        console.error(error);
        alert("Failed to delete book");
      });
}


function showData(data) {
    var ne = JSON.parse(data)

    document.querySelector(".im").src = ne.uimg
    
}

