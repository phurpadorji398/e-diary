window.onload = function (){
    alert("hi")
    var email = localStorage.getItem("email")
    fetch("/getDiary/"+email)
    .then(resp => resp.text())
    .then(data => showData(data))
    .catch(e =>{
        alert(e)
    })
}

function showData(data){
    console.log(data)
    const diaries = JSON.parse(data)
    console.log(diaries)
    diaries.forEach(element => {
        newDiary(element)
    });
}

function newDiary(element){
    // var listItem = document.createElement('li');
    //   listItem.classList.add('entry');
    //   listItem.innerHTML = '<h3>' + element.date + '</h3><p>' + element.content + '</p>' +
    //     '<button class="edit-button" onclick="editEntry(' + index + ')">Edit</button>' +
    //     '<button class="delete-button" onclick="deleteEntry(' + index + ')">Delete</button>';
    //   diaryList.appendChild(listItem);
    const div = document.createElement("div")
    div.setAttribute("class","jabkha")
    const h = document.createElement("h1")
    h.setAttribute("class","heading")
    h.innerHTML = element.Date.split("T")[0]
    const p = document.createElement("textarea")
    p.setAttribute("class","content")
    p.innerHTML = element.Content
    const button1 = document.createElement("button")
    button1.addEventListener("click",function(){deletes(element.Id)})
    button1.innerHTML = "Delete"
    const button2 = document.createElement("button")
    button2.innerHTML = "Edit"
    button2.addEventListener("click",function(){edit(element.Id)})
    div.appendChild(h)
    div.appendChild(p)
    div.appendChild(button1)
    div.appendChild(button2)
    document.querySelector(".outside").appendChild(div)

      // Apply styles
  div.style.display = "flex";
  div.style.flexDirection = "column";
  div.style.alignItems = "center";
  div.style.color = "white";
  div.style.gap = "10px";
  p.style.resize = "none";
  p.style.width = "90%";
  p.style.height = "50px";
  p.style.color = "white";
  button1.style.alignSelf = "center";
  button2.style.alignSelf = "center";
  button1.style.padding = "10px"; // Increase padding for better button size
  button2.style.padding = "10px"; // Increase padding for better button size
  button1.style.fontSize = "16px"; // Adjust font size as needed
  button2.style.fontSize = "16px"; 


}

function edit(id){
    localStorage.setItem("id",id)
    window.location.href = "update.html"
}

function deletes(id){
    fetch("/delete/"+id,{
        method:"DELETE",
        headers:{"content-type":"application/json; charset=utf-8"}
    })
    .then(resp =>{
        if(resp.ok){
            window.location.href = "read.html"
        }else{
            throw new Error(resp.statusText)
        }
    })
    .catch(e =>{
        alert(e)
    })
}