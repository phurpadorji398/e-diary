var email = localStorage.getItem("email")
function savePage(){
    const currentTime = new Date();
    console.log(currentTime);
    var data = {
        Date : currentTime,
        Content : document.getElementById("myTextarea").value,
        Email :email
    }
    fetch("/save",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json; charset=utf-8"}
    })
    .then(resp =>{
        if (resp.ok){
            window.location.href = "day.html"
        }else{
            throw new Error(resp.statusText)
        }
    })
    .catch(e =>{
        alert(e)
    })
}