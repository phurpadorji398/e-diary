function login() {
    alert("hi")
    var _data = {
        email: document.getElementById("email").value,
        password: document.getElementById("pw").value
    }
    fetch('/Login', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    }).then(response => {
        console.log(response)
        if (response.ok) {
            localStorage.setItem("email",document.getElementById("email").value)
            window.open("home.html", "_self")
        } else {
            throw new Error(response.statusText)
        }
    }).catch(e => {
        if (e == "Error: Unauthorized" || e == " ") {
            alert(e + ". Credentials does not match!")
            return
        }
    });
}


function logout(){
    fetch("/logout")
    .then(resp =>{
        if(resp.ok){
            window.location.href = "index.html"
        }else{
            throw new Error(resp.statusText)
        }
    })
    .catch(e =>{
        alert(e)
    })
   
}