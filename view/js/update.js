var id = localStorage.getItem("id")
window.onload = function(){
    fetch("/getDiarry/"+id)
    .then(resp => resp.text())
    .then(data => showDiary(data))
    .catch(e =>{
        alert(e)
    })
}

function showDiary(data){
    console.log(data)
    const diary = JSON.parse(data)
    document.getElementById("myTextarea").innerHTML= diary.Content
}


function Update(){
    var data = {
        Content : document.getElementById("myTextarea").value
    }
    fetch("/updateDiary/"+id,{
        method:"PATCH",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json;charset=utf-8"}
    })
    .then(resp =>{
        if(resp.ok){
            window.location.href = "read.html"
        }else{
            throw new Error(resp.statusText)
        }
    }).catch(e =>{
        alert(e)
    })
}