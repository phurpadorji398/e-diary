create table diarry(
    id serial not null primary key,
    date varchar(45) not null,
    content text default null,
    email varchar(45) not null,
    foreign key (email) references user_diary(email) on update cascade on delete cascade
)

DROP TABLE diarry;
DROP TABLE user_diary;

CREATE TABLE user_diary (
    name varchar(30) not null,
    email varchar(40) PRIMARY KEY,
    password varchar(30) not null, 
    job varchar(40),
    uimg text
)