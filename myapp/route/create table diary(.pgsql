create table diary(
    time varchar(5) not null,
    Date varchar(45) not null,
    Content text,
    PRIMARY KEY(time)
)

truncate diary;

drop table diary;