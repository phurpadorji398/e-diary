package routes

import (
	controller "E-Dairy/myapp/controllers"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()
	//route for signup
	router.HandleFunc("/signup", controller.AddUserHandler).Methods("POST")
	router.HandleFunc("/Login", controller.Login).Methods("POST")
	router.HandleFunc("/logout",controller.LogoutHandler).Methods("GET")
	router.HandleFunc("/user/{email}", controller.UpdateUserHandler).Methods("PUT")

	router.HandleFunc("/profile/{email}", controller.ProfileHandler).Methods("GET")

	router.HandleFunc("/save",controller.CreateDiary).Methods("POST")
	router.HandleFunc("/getDiary/{email}",controller.GetDiary).Methods("GET")
	router.HandleFunc("/getDiarry/{id}",controller.GetSpecificDiary).Methods("GET")
	router.HandleFunc("/delete/{id}",controller.DeleteDiary).Methods("DELETE")
	router.HandleFunc("/updateDiary/{id}",controller.Updatecontent).Methods("PATCH")
	// to serve static file
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	err := http.ListenAndServe(":8082", router)
	log.Println("Application is running on port 8081........")
	if err != nil {
		fmt.Println("problem with listenandserve")
		fmt.Print(err)
		os.Exit(1)
	}

}
