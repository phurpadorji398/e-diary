package model

import (
	"E-Dairy/myapp/datastore/postgres"
)
type Diary struct {
	Id int
	Date string 
	Content string
	Email string 
}


func (d *Diary) Creatediary()error {
	const queryCreate = "INSERT INTO diarry(date,content,email) VALUES($1,$2,$3);"
	_,err := postgres.Db.Exec(queryCreate,d.Date,d.Content,d.Email)
	return err
}

func (d *Diary) DeleteDiary(a string)error {
	const queryDeleteDiary = "DELETE FROM diarry WHERE id = $1 RETURNING id;"
	err := postgres.Db.QueryRow(queryDeleteDiary,a).Scan(&d.Id)
	return err
}

func (d *Diary)GetDiaryData(email string)([]Diary,error) {
	const query = "select * from diarry where Email = $1;"
	rows, getErr := postgres.Db.Query(query,email)
	if getErr != nil{
		return nil,getErr
	}
	diaries := []Diary{}
	for rows.Next(){
		var d Diary
		dbErr := rows.Scan(&d.Id,&d.Date,&d.Content,&d.Email)
		if dbErr != nil{
			return nil, dbErr
		}
		diaries = append(diaries,d)
	}
	rows.Close()
	return diaries,nil
}
func (d *Diary) UpdateDiary(id int) error {
	const queryUpdate = "UPDATE diarry SET content = $1 WHERE id = $2;"
	_, err := postgres.Db.Exec(queryUpdate, d.Content, id)
	return err
}
func (d *Diary) GetDiaryBook(id int)error{
	const query = "select * from diarry where id = $1;"
	return postgres.Db.QueryRow(query,id).Scan(&d.Id,&d.Date,&d.Content,&d.Email)
}
// func (d *Diary) UpdateDiary(t string) error {
// 	const queryUpdate = "UPDATE diary SET Content = $1 WHERE time = $2;"
// 	_, err := postgres.Db.QueryRow(queryUpdate, d.Content, d.time,t).Scan(&d.time)
// 	return err
// }