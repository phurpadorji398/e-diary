package model

import (
	"E-Dairy/myapp/datastore/postgres"
	"database/sql"
	"fmt"
	"log"
)

type User struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Job string `json:"job"`
	Uimg string `json:"uimg"`
}

//db details

const (
	postgres_host     = "db"
	postgres_port     = 5432
	postgres_user     = "postgres"
	postgres_password = "postgres"
	postgres_dbname   = "my_db"
)

//create pointer variable Db which points to sql driver

var Db *sql.DB

// init() is always called before main()

func init() {
	//creating the connection string
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error
	//open connection to database

	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}

func (a *User) Create() error {
	const queryCreateUser = "INSERT INTO user_diary (name,email,password, job,uimg) VALUES($1,$2,$3, $4, $5)"
	_, err := postgres.Db.Exec(queryCreateUser, a.Name, a.Email, a.Password,a.Job, a.Uimg)
	return err
}

const queryGetAdmin = "SELECT email, password FROM user_diary WHERE email=$1 and password=$2;"

func (a *User) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, a.Email, a.Password).Scan(&a.Email, &a.Password)
}
func (u *User) GetProfile(email string) error {
	const queryGetProfile = "Select * from user_diary where email = $1;"
	err := postgres.Db.QueryRow(queryGetProfile, email).Scan(&u.Name, &u.Email, &u.Password, &u.Job, &u.Uimg)
	return err
}

func (u *User) UpdateUser(email string) error {
	const queryUdateUser = "UPDATE user_diary set name=$1, job=$2,uimg=$3 WHERE email = $4 RETURNING Email"
	err := postgres.Db.QueryRow(queryUdateUser, u.Name, u.Job,u.Uimg, email).Scan(&u.Email)
	fmt.Println(err, "here")
	return err
}
