package controller

// import (
// 	"encoding/json"
// 	"fmt"
// 	"myapp/model"

// 	"myapp/utils/httpResponse"

// 	"net/http"
// )
import (
	"E-Dairy/myapp/model"
	httpResponse "E-Dairy/myapp/utils/httpResponse"
	"database/sql"
	"encoding/json"
	"time"

	"fmt"

	"net/http"

	"github.com/gorilla/mux"
)

func AddUserHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	saveErr := user.Create()
	if saveErr != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	fmt.Println(user)
	httpResponse.ResponseWithJson(w, http.StatusCreated, user)
	fmt.Println(http.StatusCreated)
}

func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("reaching here")
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		fmt.Println("q",err)
		return
	}
	defer r.Body.Close()
	getErr := user.Get()
	if getErr != nil {
		httpResponse.ResponseWithError(w, http.StatusUnauthorized, getErr.Error())
		fmt.Println("2",getErr)
		return
	}
	cookie := http.Cookie{
		Name:    "user-cookie",
		Value:   "#@Furpa77",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	httpResponse.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "success"})
	fmt.Println("dfkkjkk")
}
func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("reaching here")
	email := mux.Vars(r)["email"]
	var user model.User
	getErr := user.GetProfile(email)
	if getErr != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "error in getting from database")
		fmt.Println("couldn't get from database")
		return
	}
	httpResponse.ResponseWithJson(w, http.StatusOK, user)
	fmt.Println("result", user)
}

func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var user model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	dbErr := user.UpdateUser(email)
	if dbErr != nil {
		switch dbErr {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w, http.StatusNotFound, "User not found")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, dbErr.Error())
		}
	} else {
		httpResponse.ResponseWithJson(w, http.StatusOK, user)
	}
}
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("kkkkkkkk")
	http.SetCookie(w, &http.Cookie{
		Name:    "user-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	httpResponse.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "logout successful"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("user-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			httpResponse.ResponseWithError(w, http.StatusSeeOther, "cookie not set")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, "internal server error")
		}
		return false
	}
	if cookie.Value != "#@Furpa77" {
		httpResponse.ResponseWithError(w, http.StatusSeeOther, "invalid cookie")
		return false
	}
	return true
}
