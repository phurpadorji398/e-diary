package controller

import (
	"E-Dairy/myapp/model"
	"E-Dairy/myapp/utils/httpResponse"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func CreateDiary(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	var diary model.Diary

	err :=json.NewDecoder(r.Body).Decode(&diary)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest,"invalid json body")
		fmt.Println("error in decoding")
		return 
	}

	updateErr := diary.Creatediary()
	if updateErr != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest,updateErr.Error())
		fmt.Println("error in inserting the data",updateErr)
		return
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,map[string]string{"message":"successful"})
	fmt.Println("successful")

}

func DeleteDiary(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r) {
		return
	}
	date := mux.Vars(r)["id"]
	fmt.Println("hellow iii",date)
	fmt.Println(date)
	var diary model.Diary

	err := diary.DeleteDiary(date)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w,http.StatusNotFound,"diary not found")
			fmt.Println("diary not found")
		default:
			httpResponse.ResponseWithError(w,http.StatusBadRequest,"error with database")
			fmt.Println("error from datbase",err)
		}
		return
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,map[string]string{"message":"successful"})
	fmt.Println("successfully deleted")
}
func Updatecontent(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r) {
		return
	}
	// time:=mux.Vars(r)["time"]
	id := mux.Vars(r)["id"]
	Id,Cerr := strconv.Atoi(id)
	if Cerr != nil{
		fmt.Println("error in converting integer")
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"error in coverting int")
		return 
	}
	var Context model.Diary
	decoder:=json.NewDecoder(r.Body)
	err:=decoder.Decode(&Context)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest,"invalid json body")
		fmt.Println("error in decoding")
		return 
	}

	updateerr:=Context.UpdateDiary(Id)
	if updateerr !=nil{
		switch updateerr {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w,http.StatusNotFound,"diary not found")
			fmt.Println("diary not found")
		default:
			httpResponse.ResponseWithError(w,http.StatusBadRequest,"error with database")
			fmt.Println("error from datbase",updateerr)
		}
		return
	}
	httpResponse.ResponseWithJson(w,http.StatusOK, "updated")

}

func GetDiary(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r) {
		return
	}
	email := mux.Vars(r)["email"]
	fmt.Println("hi",email)

	var diary model.Diary
	diaries,err := diary.GetDiaryData(email)
	if err != nil{
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"error in inserting the data")
		fmt.Println("error in inserting",err)
	}else{
		httpResponse.ResponseWithJson(w,http.StatusOK,diaries)
		fmt.Println("success",diaries)
	}
}

func GetSpecificDiary(w http.ResponseWriter,r *http.Request){
	if !VerifyCookie(w, r) {
		return
	}
	id := mux.Vars(r)["id"]
	Id,cErr := strconv.Atoi(id)
	fmt.Println("minanga opha",id)
	if cErr != nil{
		fmt.Println("converting error",cErr)
		httpResponse.ResponseWithError(w,http.StatusBadRequest,"error in converting id")
		return 
	}
	var diary model.Diary
	err := diary.GetDiaryBook(Id)
	if err != nil{
		fmt.Println("errorn",err)
		return 
	}
	httpResponse.ResponseWithJson(w,http.StatusOK,diary)
	fmt.Println("success",diary)
}